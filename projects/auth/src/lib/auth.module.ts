import { NgModule } from '@angular/core';
import {LoginComponent} from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {FlexModule} from '@angular/flex-layout';
import {MatButtonModule, MatFormFieldModule, MatInputModule, MatTabsModule} from '@angular/material';
import {AuthRoutingModule} from './auth-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AuthComponent} from './auth/auth.component';
import {RegisterComponent} from './register/register.component';

const matModules = [
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatTabsModule
];

@NgModule({
  declarations: [
    LoginComponent,
    AuthComponent,
    RegisterComponent
  ],
  imports: [
    ...matModules,
    BrowserAnimationsModule,
    FormsModule,
    CommonModule,
    FlexModule,
    ReactiveFormsModule,
    AuthRoutingModule
  ],
  exports: [
    AuthComponent
  ]
})
export class AuthModule { }
