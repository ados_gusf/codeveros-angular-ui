// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

try {
  const puppeteer = require('puppeteer');
  process.env.CHROME_BIN = puppeteer.executablePath();
  console.log('Using Puppeteer provided Headless Chrome');
} catch (e) {
  console.log('Puppeteer not installed, trying to use separately installed Chrome');
}

module.exports = function (config) {
  config.set({
    browserDisconnectTimeout: 20000,
    browserNoActivityTimeout: 60000,
    browserDisconnectTolerance: 5,
    retryLimit: 5,
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-junit-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, '../../reports/coverage/user'),
      reports: ['html', 'lcovonly', 'text-summary'],
      fixWebpackSourcePaths: true
    },
    junitReporter: {
      outputDir: require('path').join(__dirname, '../../reports/test/user'),
      outputFile: 'test-results.xml'
    },
    reporters: ['progress', 'junit'],
    port: 9875,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['CustomChrome'],
    customLaunchers: {
      CustomChrome: {
        base: 'ChromeHeadless',
        flags: [
          '--no-sandbox',
          '--disable-gpu',
          '--disable-web-security',
          '--disable-setuid-sandbox',
          '--remote-debugging-port=9222',
        ]
      }
    },
    singleRun: true
  });
};
