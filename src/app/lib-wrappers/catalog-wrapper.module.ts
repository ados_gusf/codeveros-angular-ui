import {TrainingModule} from 'catalog';
import {NgModule} from '@angular/core';

@NgModule({
  imports: [TrainingModule]
})
export class CatalogWrapperModule {}
