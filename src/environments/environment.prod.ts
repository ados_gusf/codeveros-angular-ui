export const environment = {
  production: true,
  catalogEndpoint: 'api/training',
  userEndpoint: 'api/user'
};
